package ru.tusur.rss.wicket.data;

import org.apache.wicket.markup.html.WebPage;

import java.awt.*;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.extensions.markup.html.repeater.data.sort.OrderByBorder;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.model.AbstractReadOnlyModel;
import ru.tusur.rss.domain.NodeActivity;
import ru.tusur.rss.domain.XBeeNode;
import ru.tusur.rss.services.NodeService;
import ru.tusur.rss.services.PortService;

import java.text.SimpleDateFormat;
import java.util.Date;


public class MyPage extends ExamplePage {

    public MyPage() {
        add(new Label("message", "Hello World!"));
    }
}
