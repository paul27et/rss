package ru.tusur.rss.services;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.tusur.rss.dao.ActivitiesRepository;
import ru.tusur.rss.domain.Frame;
import ru.tusur.rss.domain.NodeActivity;
import ru.tusur.rss.domain.XBeeNode;

import javax.annotation.PostConstruct;
import java.io.InterruptedIOException;
import java.nio.IntBuffer;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;


import static jssc.SerialPort.*;

/**
 * @author Vladimir Vagaytsev
 */
@Service("portService")
@DependsOn({"liquibase"})
public class PortService {

    private static final Logger log = LoggerFactory.getLogger(PortService.class);

    /**
     * The maximum size of the receive buffer.
     */
    private static final int INPUT_BUFFER_SIZE = 512;

    /**
     * An internal receive buffer.
     */
    private IntBuffer buffer;


    @Value("${read.timeout}")
    long readTimeout;

    @Value("${serial.port}")
    String portName;

    @Value("${serial.speed}")
    int portSpeed;

    @Autowired
    ActivitiesRepository activitiesRepository;

    //todo: кэширование запросов к репозиторию для ускорения работы страницы

    /**
     * A synchronization object used to wait for incoming data in port.
     */
    private final Object available = new Object();
    private static SerialPort port;
    static byte[] cmdND = {0x7E, 0x00, 0x04, 0x08, 0x01, 0x4E, 0x44, 0x64};

    @Scheduled(initialDelay = 1000, fixedDelayString = "${discover.delay}")
    public void discoverNodes() {
        synchronized (available) {
            log.info("in available discoverNodes!");
            try {
                write(cmdND);
            } catch (SerialPortException e) {
                e.printStackTrace();
            } catch (InterruptedIOException e) {
                e.printStackTrace();
            }
        }
    }

    public void write(byte[] writeBuffer) throws SerialPortException, InterruptedIOException {
        synchronized (available) {
            SerialPort localPort = getPort();
            if (localPort != null) {
                log.info("in available while write");
                localPort.purgePort(PURGE_RXCLEAR | PURGE_TXCLEAR);
                localPort.writeBytes(writeBuffer);
            } else {
                log.warn("Serial port is null. portName == {}", portName);
            }
        }
    }

    @PostConstruct
    void initPort() {
        try {
            port = new SerialPort(portName);
            port.openPort();
            port.setParams(portSpeed, DATABITS_8, STOPBITS_1, PARITY_NONE);
            int mask = SerialPort.MASK_RXCHAR + SerialPort.MASK_CTS + SerialPort.MASK_DSR;//Prepare mask
            port.setEventsMask(mask);//Set mask
            port.addEventListener(new SerialPortReader());
            byte[] initSeq = new byte[]{0x0d, 0x42};
            byte[] bstr = {0x7e, 0x00, 0x04, 0x08, 0x01, 0x4e, 0x49, 0x5f};
            try {
                write(initSeq);
            } catch (InterruptedIOException e) {
                log.info("time out after init sequence");
                e.printStackTrace();
            }
            try {
                write(bstr);
            } catch (InterruptedIOException e) {
                log.info("time out after init sequence");
                e.printStackTrace();
            }

            buffer = IntBuffer.allocate(INPUT_BUFFER_SIZE);
        } catch (SerialPortException ex) {
            log.error("!!!!!!!!!!!!!!!!!!!!  cannot initialize port : {} : {}", portName, ex);
            port = null;
        }
    }


    static BlockingDeque<Frame> synchronizedQueue = new LinkedBlockingDeque<>();
    static Frame lastFrame;

    @Scheduled(initialDelay = 5000, fixedDelayString = "${dispatch.delay}")
    private void dispatchFrames() {
        byte[] buffer = new byte[]{0x7E, 0x00, 0x0A, (byte) 0x88, 0x01, 0x4E, 0x49, 0x00,
                0x65, 0x6E, 0x64, 0x64, 0x32, 0x12};
        SerialPortReader spr = new SerialPortReader();
        spr.createFrame(buffer, "/dev/Fake/port");

        for (; !synchronizedQueue.isEmpty(); ) {
            Frame frame = null;
            try {
                frame = synchronizedQueue.take();
                process(frame);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (Exception e) {
                log.error("exception while process frame : {},: {}", frame, e);
            }
        }
    }

    @Autowired
    NodeService nodeService;

    private void process(Frame frame) {

        log.info("process frame : {}", getBytesAsHexString(frame.getFrameBytes()));
        if (frame.check()) {
            log.info("check {}", frame.check());
            NodeActivity activity = new NodeActivity();
            activity.setNodeId(getNodeID(frame));
            activity.setNodeName(getNodeName(frame));
            activity.setPortName(frame.getPortName());
            activity.setResponseFrame(frame.getFrameBytes());
            activity.setFrameType(frame.getFrameType());
            activity.setAtCommand(frame.getAtCommand());
            activity.setUpdateTimeStamp(System.currentTimeMillis());
//            activitiesRepository.save(activity);
            XBeeNode xBeeNode = null;

            if (nodeService.hasNode(getNodeID(frame))) {
                xBeeNode = nodeService.getNode(getNodeID(frame));
                xBeeNode.setUpdateTimeStamp(System.currentTimeMillis());
                xBeeNode.getActivities().add(activity);
            } else {
                List<NodeActivity> activities = new LinkedList<>();
                activities.add(activity);
                xBeeNode = new XBeeNode(activity.getNodeId(), getIdBytes(frame), System.currentTimeMillis(), System.currentTimeMillis(), activities);
            }

            if (StringUtils.isNotEmpty(activity.getNodeName())) {
                xBeeNode.setNodeName(activity.getNodeName());
            }

            if (!"0000000000000000".equals(getNodeID(frame))) {
                nodeService.save(xBeeNode);
            } else {
                log.warn("ignoring add node with id : 0000000000000000, saving activities");
                activitiesRepository.save(activity);
            }
        } else {
            log.warn("check not valid {}, frame dropped", frame.getChk());
        }
    }

    private byte[] getIdBytes(Frame frame) {
        byte[] result = new byte[8];
        if ("ND".equalsIgnoreCase(frame.getAtCommand())) {
            result = frame.getAddrFromNDbytes();
        }
        return result;
    }

    private String getNodeName(Frame frame) {
        if (frame.getFrameType() == 0x88) {
            if ("ND".equalsIgnoreCase(frame.getAtCommand())) {
                return frame.getNiFromND();
            } else if ("NI".equalsIgnoreCase(frame.getAtCommand())) {
                return frame.getNiFromNI();
            }
        }
        return "";
    }

    String getNodeID(Frame frame) {



        if (frame.getFrameType() == 0x88) {
            if ("NI".equalsIgnoreCase(frame.getAtCommand())) {
                return "local";
            }
            if ("ND".equalsIgnoreCase(frame.getAtCommand())) {
                log.info("frame.getSize() => {}", frame.getSize());
                log.info(getBytesAsHexString(frame.getFrameBytes()));
                log.info("frame.getFrameBytes() {}", new String(frame.getFrameBytes()));
                log.info(getBytesAsHexString(frame.bytesFromBytes(frame.getCommandData())));
                log.info(frame.getNiFromND());
                log.info(frame.getAddrFromND());

                return frame.getAddrFromND();
            } else {
                return frame.getAddrFromND();
            }
        }

        if (frame.getFrameType() == 0x97) {
            if (!"IS".equalsIgnoreCase(frame.getRemoteAtCommand())) {
                log.info("remote AT : {}", frame.getRemoteAtCommand());
            }
            return frame.getRemoteAddr();
        }

        return "unknown";

    }

    public SerialPort getPort() {
        if (port == null) {
            initPort();
        }
        return port;
    }

    public static void setPort(SerialPort port) {
        PortService.port = port;
    }

    public static String getBytesAsHexString(byte[] bytes) {

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(String.format("%02X", bytes[i]));
        }

        return sb.toString();

    }

    public void pollIsCmd(Map<String, XBeeNode> nodes) {
        for (String s : nodes.keySet()) {
            if (!nodes.get(s).getZigBeeId().equalsIgnoreCase("local")) {
                log.info("issuing : {} {}", s, nodes.get(s).getZigBeeId());
                issueIScmd(nodes.get(s));
            } else {
                log.info("ignoring : {} {}", s, nodes.get(s).getZigBeeId());
            }
        }
    }

    private void issueIScmd(XBeeNode xBeeNode) {
        try {
            log.info(getBytesAsHexString(createIScmd(xBeeNode)));
            write(createIScmd(xBeeNode));

        } catch (SerialPortException | InterruptedIOException e) {
            e.printStackTrace();
            log.error("error while writing cmd {}", e);
        }
    }

    public static byte[] createIScmd(XBeeNode xBeeNode) {
        byte[] result = new byte[]{0x7E, 0x00, 0x0F, 0x17, 0x01
                //  - Start delimiter: 7E,
                // - Length: 00 0F (15)
                // - Frame type: 17 (Remote AT Command Request)
                // - Frame ID: 01 (1)
                , 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 64-bit remote addr
                (byte) 0xFF, (byte) 0xFE, // 16-bit remote addr - always
                0x02, // command options
                0x49, 0x53,    // IS command
                0x00}; // initial chk sum - must be changed

        for (int i = 0; i < xBeeNode.idBytes.length; i++) {
            byte idByte = xBeeNode.idBytes[i];
            result[i + 5] = idByte;
        }

        IntBuffer intBuffer = IntBuffer.allocate(result.length);

        for (Byte aByte : result) {
            intBuffer.put(Frame.unsignedToBytes(aByte));
        }

        result[result.length - 1] = (byte) Frame.countChk(intBuffer);
        intBuffer.put(result.length - 1, (byte) Frame.countChk(intBuffer));
        log.info("chk in is cmd :" + Frame.check(intBuffer));
        return result;
    }

    static class SerialPortReader implements SerialPortEventListener {

        private final Object available = new Object();

        public void serialEvent(SerialPortEvent event) {
            synchronized (available) {
                if (event.isRXCHAR()) {
                    if (event.getEventValue() > 0) {
                        try {
                            byte buffer[] = port.readBytes(event.getEventValue());
                            StringBuilder sb = new StringBuilder();
                            for (byte b : buffer) {
                                sb.append(String.format("%02X ", b));
                            }
                            log.info(sb.toString());
                            createFrame(buffer, port.getPortName());

                        } catch (SerialPortException ex) {
                            System.out.println(ex);
                        }
                    }
                } else if (event.isCTS()) {//If CTS line has changed state
                    if (event.getEventValue() == 1) {//If line is ON
                        System.out.println("CTS - ON");
                    } else {
                        System.out.println("CTS - OFF");
                    }
                } else if (event.isDSR()) {///If DSR line has changed state
                    if (event.getEventValue() == 1) {//If line is ON
                        System.out.println("DSR - ON");
                    } else {
                        System.out.println("DSR - OFF");
                    }
                }
            }
        }

        void createFrame(byte[] buffer, String portName) {
            if (lastFrame == null) {
                lastFrame = new Frame(buffer, portName);
            } else {
                lastFrame.append(buffer);
            }

            if (lastFrame.isFinished()) {
                synchronizedQueue.add(lastFrame);
                if (lastFrame.hasSuffix()) {
                    byte[] suffix = lastFrame.getSuffix();
                    lastFrame = null;
                    createFrame(suffix, portName);
                }
                lastFrame = null;
            }
        }
    }
}