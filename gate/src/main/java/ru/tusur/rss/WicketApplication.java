package ru.tusur.rss;

import org.apache.wicket.response.filter.ServerAndClientTimeFilter;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.wicket.Page;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import ru.tusur.rss.dao.ActivitiesRepository;
import ru.tusur.rss.wicket.data.MyPage;
import ru.tusur.rss.wicket.data.SortingDataPage;

/**
 * Created by saturn on 29.01.14.
 */
public class WicketApplication extends WebApplication implements ApplicationContextAware {
    private static final Logger log = LoggerFactory.getLogger(WicketApplication.class);

    ApplicationContext applicationContext;

    @Autowired
    @SpringBean
    ActivitiesRepository activitiesRepository;

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    /**
     * Constructor.
     */
    public WicketApplication()
    {
    }

    /**
     * @see org.apache.wicket.protocol.http.WebApplication#init()
     */
    @Override
    protected void init()
    {
        getDebugSettings().setDevelopmentUtilitiesEnabled(true);

        getRequestCycleSettings().addResponseFilter(new ServerAndClientTimeFilter());

        getMarkupSettings().setStripWicketTags(true);

        getComponentInstantiationListeners().add(new SpringComponentInjector(this, applicationContext));

        mountPage("/sort", SortingDataPage.class);

        mountPage("/my", MyPage.class);
    }

    /**
     * @return contacts database
     */
    public ActivitiesRepository getActivitiesDB()
    {
        return activitiesRepository;
    }

    /**
     * @see org.apache.wicket.Application#getHomePage()
     */
    @Override
    public Class<? extends Page> getHomePage()
    {
        return SortingDataPage.class;
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }


    /*ApplicationContext applicationContext;

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    @Override
    public Class<? extends Page> getHomePage() {
        return MainPage.class;
    }

    WicketApplication () {
        super();

    }

    public void init() {
        getJavaScriptLibrarySettings().setJQueryReference(new UrlResourceReference(Url.parse("/js/jquery-2.0.2.min.js")));
        getComponentInstantiationListeners().add(new SpringComponentInjector(this, applicationContext));
        mountPage("/index.html", MainPage.class);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }*/
}

