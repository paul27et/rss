package ru.tusur.rss.dao;

import org.springframework.data.repository.CrudRepository;
import ru.tusur.rss.domain.NodeActivity;

import java.util.List;

public interface ActivitiesRepository extends CrudRepository<NodeActivity, Integer> {

    List<NodeActivity> ordered();
}