package ru.tusur.rss.dao;

import org.springframework.data.repository.CrudRepository;
import ru.tusur.rss.domain.XBeeNode;

public interface XBeeNodeRepository extends CrudRepository<XBeeNode, String> {

}