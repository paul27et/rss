package ru.tusur.rss.services;

import com.google.common.base.Preconditions;
import org.apache.wicket.examples.repeater.Index;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import ru.tusur.rss.GateContext;
import ru.tusur.rss.wicket.MainPage;
import ru.tusur.rss.wicket.data.SortingDataPage;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = GateContext.class,
        loader = AnnotationConfigContextLoader.class)
@ActiveProfiles("test")
public class PagesTest  extends AbstractJUnit4SpringContextTests {

    private static WicketTester tester;

    @Test
    public void mainPageTest() throws Exception {
        tester.startPage(MainPage.class);
        tester.assertRenderedPage(Index.class);
    }

    @Test
    public void sortingPageTest() throws Exception {
        tester.startPage(SortingDataPage.class);
        tester.assertRenderedPage(SortingDataPage.class);
    }

    @Before
    public void init() {

        getTester();

    }

    WicketTester getTester() {

        if (tester == null) {
            WebApplication application = (WebApplication) applicationContext.getBean("wicketApplication");
            Preconditions.checkNotNull(application);
            tester = new WicketTester(application);
        }
        return tester;

//        if (tester == null) {
//            tester = new WicketTester();
//        }
//        return tester;
    }
}